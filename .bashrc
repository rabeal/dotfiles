#
# ~/.bashrc
#
#Import cholorscheme from 'wal'
(cat ~/.cache/wal/sequences &)

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

if [[ $(ps --noheader --pid=$PPID --format=cmd) != "fish" ]]
then 
        exec fish
fi
source "$HOME/.cargo/env"
