#Start X at login
#if status --is-interactive
#    exec startx
#end

#Import colorscheme from 'wal' asynchronously
#& #Run the process in the background.
#() #Hide shell job control messages.
#(cat ~/.cache/wal/sequences &)
#
#Alternative (blocks terminal for 0-3ms)
cat  ~/.cache/wal/sequences

#Add support for TTYs
#source ~/.cache/wal/colors-tty.sh

#setting up rust with rustup
export PATH="$HOME/.cargo/bin:$PATH"
