#!/bin/bash

#Fonts
Panel_Font="Font Awesome:size=10"
#colors
Color_White="FFFFFF"
Color_Black="#000000"
Color_Dark_Grey="#777777"
Color_Light_Grey="#CCCCCC"
Color_Pink="#F00AC1"
Color_Teal="#03B4CB"
Color_Yellow="#FFF331"
Color_Mint_Green="#67FFA9"
Color_Lilac="#B19EFC"
Spacer(){

  echo "%{F$Color_Lilac}|"
}

Battery(){
  BATPERC=$(acpi --battery | cut -d, -f2)
  echo -e "$BATPERC $(Spacer)"
}
Date(){
  DATE=$(date "+%d.%m.%y")
  echo -n "$DATE $(Spacer)"
}

Time(){
  TIME=$(date "+%H:%M")
  echo -n "$TIME $(Spacer)"
}


Update(){

  echo -e "%{l} $(Spacer) %{F$Color_Mint_Green} $(Date) %{F$Color_Yellow} $(Time) %{F$Color_Pink} $(Battery)" 
}

while true; do
        Update
        sleep 1s &
        wait
done

